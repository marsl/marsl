<?php
include_once (dirname(__FILE__)."/../../includes/errorHandler.php");
?>
<li>
	<a class="hide" <?php if ($cat_type == 0): ?>href="#"<?php endif; ?><?php if ($cat_type == 1): ?>href="index.php?var=urlloader&amp;id=<?php echo $cat_id; ?>"<?php endif; ?>>
		<?php echo $cat_name; ?>
	</a>
	<ul>
		<?php foreach ($links as $link): ?>
		<li>
			<a href="index.php?var=urlloader&amp;id=<?php echo $link['id']; ?>">
				<?php echo $link['name']; ?>
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</li>